/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.exhaustivesearch;

/**
 *
 * @author admin
 */
public class ExhaustiveSearchTest {

    public static void main(String[] args) {
        int[] n = {1, 2, 3, 5, 4, 8, 7};

        showInput(n);
        ExhaustiveSearch exhaustive = new ExhaustiveSearch(n);
        exhaustive.process();
        exhaustive.sum();
    }

    private static void showInput(int[] n) {
        System.out.print("Input is: ");
        for (int i = 0; i < n.length; i++) {
            System.out.print(n[i] + " ");
        }
        System.out.println("");
    }

}
