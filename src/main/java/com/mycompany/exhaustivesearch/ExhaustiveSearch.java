/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.mycompany.exhaustivesearch;

import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class ExhaustiveSearch {

    private int[] number;
    static int indexArray = 0;
    static ArrayList<ArrayList<Integer>> prepare = new ArrayList<ArrayList<Integer>>();
    static ArrayList<ArrayList<Integer>> prepare2 = new ArrayList<ArrayList<Integer>>();

    public ExhaustiveSearch(int[] num) {
        this.number = num;
    }

    static void prepareArray(int arr[], int n, int r) {
        int data[] = new int[r];
        union(arr, data, 0, n - 1, 0, r);
    }

    static void union(int arr[], int data[], int start,
            int end, int index, int r) {
        ArrayList<Integer> trash = new ArrayList<>();
        if (index == r) {
            for (int j = 0; j < r; j++) {
                trash.add(data[j]);
            }
            prepare.add(indexArray, trash);
            return;
        }

        for (int i = start; i <= end && end - i + 1 >= r - index; i++) {
            data[index] = arr[i];
            union(arr, data, i + 1, end, index + 1, r);
        }
    }

    public void process() {
        ArrayList<Integer> trash1 = new ArrayList<>();
        ArrayList<Integer> trash2 = new ArrayList<>();

        for (int i = 0; i < number.length; i++) {
            trash1.add(number[i]);
        }
        for (int i = 0; i < number.length; i++) {
            trash1.add(number[i]);
        }

        int arr[] = number;
        int n = arr.length;

        for (int i = number.length; i >= 0; i--) {
            prepareArray(arr, n, i);
        }

        indexArray = 1;
        prepare2.add(0, trash2);
        for (int i = 1; i < prepare.size(); i++) {
            trash1 = new ArrayList<>();
            for (int j = 0; j < number.length; j++) {
                trash1.add(number[j]);
            }
            for (int k = 0; k < prepare.get(i).size(); k++) {
                for (int u = 0; u < trash1.size(); u++) {
                    if (trash1.get(u) == prepare.get(i).get(k)) {
                        trash1.remove(u);
                        u--;
                    }
                }
            }
            prepare2.add(indexArray, trash1);
            indexArray++;
        }
    }

    public void sum() {
        for (int i = 0; i < prepare.size(); i++) {
            if (prepare.contains(prepare2.get(i))) {
                prepare2.remove(i);
                prepare.remove(i);
            }
        }
        for (int i = 0; i < prepare.size(); i++) {
            int sum1 = 0;
            int sum2 = 0;
            for (int j = 0; j < prepare.get(i).size(); j++) {
                sum1 += prepare.get(i).get(j);
            }
            for (int j = 0; j < prepare2.get(i).size(); j++) {
                sum2 += prepare2.get(i).get(j);
            }
            if (sum1 == sum2) {
                System.out.println(prepare.get(i) + " " + prepare2.get(i));
            }
        }
    }

    public void getDistantArray1() {
        for (int i = 0; i < prepare.size(); i++) {
            for (int j = 0; j < prepare.get(i).size(); j++) {
                System.out.print(prepare.get(i).get(j) + " ");
            }
            System.out.println("");
        }
    }

    public void getDistantArray2() {
        for (int i = 0; i < prepare2.size(); i++) {
            for (int j = 0; j < prepare2.get(i).size(); j++) {
                System.out.print(prepare2.get(i).get(j) + " ");
            }
            System.out.println("");
        }
    }

}
